<?php

/**
 * @file
 * Builds placeholder replacement tokens for webform count data.
 */

/**
 * Implements hook_token_info().
 */
function webform_goal_meter_token_info() {
  // Webform node tokens.
  $info['tokens']['node']['webform-submission-total'] = [
    'name' => t('Webform Submission Total.'),
    'description' => t('The total number of submissions made to this webform.'),
  ];
  $info['tokens']['node']['webform-submission-total-percent'] = [
    'name' => t('Webform Submission Progress Bar.'),
    'description' => t('The total number of submissions as a percentage of a goal.'),
  ];
  $info['tokens']['node']['webform-submission-total-progress'] = [
    'name' => t('Webform Submission Progress Bar.'),
    'description' => t('A progress bar showing the total number of submissions as a percentage of a goal.'),
  ];

  return $info;
}

/**
 * Implements hook_tokens().
 */
function webform_goal_meter_tokens($type, $tokens, array $data = array(), array $options = array()) {

  // Generate Webform tokens.
  $replacements = array();

  if ('node' == $type && !empty($data['node']) && variable_get('webform_node_' . $data['node']->type, FALSE)) {
    module_load_include('inc', 'webform', 'includes/webform.submissions');
    $total_count = webform_get_submission_count($data['node']->nid);
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'webform-submission-total':
          $replacements[$original] = $total_count;
          break;

      }
    }
    if ($percent_tokens = token_find_with_prefix($tokens, 'webform-submission-total-percent')) {
      foreach ($percent_tokens as $name => $original) {
        $goal = floatval($name);
        $goal = (0.0 == $goal) ? 100.0 : $goal;
        $replacements[$original] = ($total_count / $goal * 100.0) . '%';
      }
    }
    if ($progress_tokens = token_find_with_prefix($tokens, 'webform-submission-total-progress')) {
      foreach ($progress_tokens as $name => $original) {
        $goal = floatval($name);
        $goal = (0.0 == $goal) ? 100.0 : $goal;
        $percentage = ($total_count / $goal) * 100.0;
        $progress_bar = '<div class="webform-submission-total-progress-bar" style="text-align:center;width:' . $percentage . '%">';
        if ($percentage > 3) {
          $progress_bar .= $percentage . '%';
        }
        $progress_bar .= '</div>';
        $replacements[$original] = '<div class="webform-submission-total-progress-container">' . $progress_bar . '</div>';
      }
    }
  }

  return $replacements;
}
