(function ($) {
  Drupal.behaviors.webformCountdown = {
    attach: function (context, settings) {
      $('.webform-goal-meter-meter span:not(.animated)').each(function () {
        $(this).prop('Counter', 0).animate({
          Counter: $(this).text()
        }, {
          duration: 2200,
          easing: 'swing',
          step: function (now) {
              $(this).text(Math.ceil(now));
          }
        });
        $(this).addClass('animated');
    });
    }
  };
})(jQuery);
