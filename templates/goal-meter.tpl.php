<?php

/**
 * @file
 * Markup for goal meter.
 *
 * Available variables:
 * - $meter_goal: the goal meter goal.
 * - $submission_total: total submissions on webform.
 * - $submission_percent: percentage of goal achieved.
 */
?>

<div class="webform-goal-meter">
  <div class="webform-goal-meter-meter">
    <span><?php print t(' @submission_total', ['@submission_total' => $submission_total]); ?></span>
    <div class="webform-goal-meter-percentage" style="width: <?php print $submission_percent;?>%;"></div>
  </div>
  <div class="webform-goal-meter-goal"><?php print t('Goal: @meter_goal', ['@meter_goal' => $meter_goal]); ?></div>
</div>
